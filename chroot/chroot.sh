#!/bin/bash

rm /etc/localtime
ln -sf /usr/share/zoneinfo/America/Bogota /etc/localtime
hwclock -w -u
nano /etc/locale.gen
locale-gen
nano /etc/pacman.conf
echo $LANG
export LANG=es_CO.UTF-8
echo LANG=es_CO.UTF-8> /etc/locale.conf
echo KEYMAP=la-latin1> /etc/vconsole.conf
read -p "Digite nombre del equipo: " ARCH
echo $ARCH> /etc/hostname
pacman -Syu networkmanager grub dialog wpa_supplicant dhclient ifplugd wpa_actiond ppp openvswitch iw --needed
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg 
mkinitcpio -p linux
passwd
read -p "Digite nombre de usuario: " USERARCH
useradd -m -g users -G audio,lp,optical,storage,video,wheel,games,power,scanner -s /bin/bash $USERARCH
passwd $USERARCH
nano /etc/sudoers
w=$(ls /sys/class/net | grep "^w")
systemctl disable dhcpcd.service
#systemctl enable netctl-auto@$w.service
rm -rf /var/lib/dhcpcd/*
#rm /etc/systemd/system/multi-user.target.wants/netctl*
rm -rf /etc/netctl/$w*