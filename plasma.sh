#!/bin/bash

sudo pacman -Sy xf86-video-ati amd-ucode xf86-input-libinput xkeycaps xorg-xmodmap pulseaudio pulseaudio-alsa libcanberra-pulse pavucontrol screenfetch dmidecode flashplugin ttf-dejavu ttf-hack ttf-ubuntu-font-family ttf-roboto arc-solid-gtk-theme arc-gtk-theme arc-icon-theme papirus-icon-theme breeze breeze-gtk breeze-icons frei0r-plugins libreoffice-fresh-es ffmpegthumbnailer fuse3 fuseiso gnome-keyring unrar p7zip zip unzip arj unarj unace lzop lzip cpio lrzip xmlto xarchiver-gtk2 alsa-utils freeglut mesa-demos git bluez bluez-utils android-tools android-udev gvfs gvfs-mtp gvfs-gphoto2 gvfs-afc jdk8-openjdk ntfs-3g gst-libav qt4 cmake bamf nfs-utils nilfs-utils ntp openconnect openvpn partclone partimage crda wireless-regdb wpa_actiond darkhttpd ddrescue dhclient dnsmasq dnsutils ethtool exfat-utils f2fs-tools fsarchiver gnu-netcat grml-zsh-config hdparm ipw2100-fw ipw2200-fw irssi lftp linux-atm lsscsi mc mtools ndisc6 ppp pptpclient refind-efi rp-pppoe rsync sdparm smartmontools tcpdump testdisk usb_modeswitch vpnc wvdial xl2tpd lsof bdf-unifont sddm sddm-kcm plasma-desktop kde-applications-meta xdg-user-dirs network-manager-applet uget  plasma-nm qbittorrent firefox firefox-i18n-es-mx vlc mpv simplescreenrecorder audacity telegram-desktop gnome-usage blueberry eclipse-java epdfview galculator-gtk2 flameshot gimp peek youtube-dl tvtime imagemagick handbrake acetoneiso2 seahorse wget kid3 htop  redshift kdenlive rhythmbox --needed
sudo systemctl enable sddm.service
w=$(ls /sys/class/net | grep "^w")
systemctl disable dhcpcd.service
rm -rf /var/lib/dhcpcd/*
rm -rf /etc/systemd/system/multi-user.target.wants/netctl*
rm -rf /etc/netctl/$w*

systemctl enable NetworkManager.service
sudo xdg-user-dirs-update